FROM openjdk:17-jdk-slim
WORKDIR /app
COPY /build/libs/surf-backend-1.0.jar /app/surf_backend.jar

ENTRYPOINT ["java"]
CMD ["-jar", "surf_backend.jar"]
