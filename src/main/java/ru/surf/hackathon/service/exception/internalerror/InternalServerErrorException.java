package ru.surf.hackathon.service.exception.internalerror;

public class InternalServerErrorException extends RuntimeException {

  public InternalServerErrorException(String message) {
    super(message);
  }
}
