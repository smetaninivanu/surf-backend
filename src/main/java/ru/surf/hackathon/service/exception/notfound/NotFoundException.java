package ru.surf.hackathon.service.exception.notfound;

public class NotFoundException extends RuntimeException {

  public NotFoundException(String message) {
    super(message);
  }
}
