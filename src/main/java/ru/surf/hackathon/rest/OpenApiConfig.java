package ru.surf.hackathon.rest;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

  @Value("${server.port}")
  private String port;

  @Bean
  public OpenAPI openAPI() {
    Server serverLocal = new Server().url("http://localhost:%s".formatted(port));
    Server serverRemote = new Server().url("https://surf.johns-last-delight.keenetic.pro");

    Info info = new Info()
      .title("Backend приложение для Surf Spring Hackathon")
      .version("v1.0");

    return new OpenAPI()
      .addServersItem(serverLocal)
      .addServersItem(serverRemote)
      .info(info);
  }
}
