package ru.surf.hackathon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SurfHackathonApplication {
  public static void main(String[] args) {
    SpringApplication.run(SurfHackathonApplication.class, args);
  }
}
