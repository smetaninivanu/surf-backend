## Backend приложение для Surf Spring Hackathon

### Запуск приложения
Вручную
1. Собрать jar файл с помощью Gradle `./gradlew build`
2. Запустить docker-compose `docker-compose up -d`

При наличии утилиты `make`
1. Запустить команду `make start`

### API приложения
- Порт по умолчанию `8086`
- Swagger API `/swagger-ui.html`